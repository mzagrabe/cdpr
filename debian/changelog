cdpr (2.4-3) unstable; urgency=medium

  * [6fac884] Fix FTBFS for GCC-10, Closes: #957072
  * [dc78df8] Bump Standards-Version
  * [4bf95df] Bump copyright year
  * [8d8c00c] Add Rules-Requires-Root stanza to d/control
  * [be26ace] Capitalize "git" in Vcs-Git stanza
  * [eec3bd8] Fix lintian warning: uses-debhelper-compat-file
  * [742e421] Quiet lintian check: maintainer-manual-page

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Fri, 24 Jul 2020 17:02:00 -0500

cdpr (2.4-2) unstable; urgency=medium

  * [895bdef] d/control replace priority extra with priority optional.
  * [202edaa] bump standards version
  * [a667541] bump debhelper compat level
  * [87086e0] remove versioned dependency for libpcap0.8-dev.
  * [811a051] update git repo to salsa.debian.org
  * [cf9615d] enable all hardening options
  * [0d80c19] comment out CFLAGS to allow dpkg-buildflags to set CFLAGS.
  * [79a3c5b] fix ftbs with ld --as-needed, closes: #632665.
  * [3900e99] Simplify Makefile, add CPPFLAGS for hardening,
    also closes: #901066.
  * [d6595fa] update URI for machine parsable copyright format 1.0
  * [cd7664e] update copyright year for debian/* files
  * [ebf169d] update GPL-2+ text in d/copyright

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Mon, 25 Jun 2018 16:02:40 -0500

cdpr (2.4-1) unstable; urgency=low

  * New upstream release, closes: #570916
  * Bumped Standards-Version to 3.9.2
  * Added ${misc:Depends} to binary depends
  * Added a Homepage entry to the source fields
  * Checked cdpr into git
  * Upstream fixed blocking interface, closes: #566983
  * Added in patch to remove pseudo device "any" from list of interfaces,
    closes: #527245
  * Updated man page. '--help' and '-?' are invalid options and hence removed
  * Updated man page. Added '-r' for read a pcap-file
  * Updated copyright file to use
    http://svn.debian.org/wsvn/dep/web/deps/dep5.mdwn?op=file&rev=174
  * Added watch file
  * Added perl cgi and php server examples
  * Added example cdpr config file, for server usage
  * Bumped debhelper compat to v8

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Thu, 05 May 2011 17:42:51 -0500

cdpr (2.2.1-1) unstable; urgency=low

  * New upstream release
  * Make sure all content gets installed when building only
    the binary-arch target, closes: #393550
  * Cleaned up manpage
  * Added upstream changelog

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Thu, 19 Oct 2006 10:33:24 -0500

cdpr (2.2.0-2) unstable; urgency=low

  * Initial release, closes: Bug#338153
  * Updated 'Standards-Version' to 3.7.2 in debian/control.
  * Cleaned up debian/rules file.

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Thu, 25 May 2006 12:40:20 -0600

cdpr (2.2.0-1) unstable; urgency=low

  * Initial release.

 -- Matt Zagrabelny <mzagrabe@d.umn.edu>  Thu,  3 Nov 2005 08:06:20 -0600
